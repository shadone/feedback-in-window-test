//
//  FeedbackViewController.swift
//  window-test
//
//  Created by Denis Dzyubenko on 18/03/2020.
//  Copyright © 2020 Shortcut. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController {
    var previousWindow: UIWindow?
    var window: UIWindow?

//    lazy var backgroundBlurView: UIVisualEffectView = {
//        let blur = UIBlurEffect(style: .light)
//        let view = UIVisualEffectView(effect: blur)
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
    lazy var backgroundBlurView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .black
        view.alpha = 0.5
        return view
    }()

    lazy var contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.clipsToBounds = true
        view.layer.cornerRadius = 8
        return view
    }()

    lazy var contentStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 16
        return stackView
    }()

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .preferredFont(forTextStyle: .title2)
        label.text = "Are you satisfied with the experience?"
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()

    lazy var slider: UISlider = {
        let slider = UISlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.minimumValue = 0
        slider.maximumValue = 10
        slider.setValue(4, animated: false)
        return slider
    }()

    lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 8
        return stackView
    }()

    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("No thanks", for: .normal)
        button.setTitleColor(.lightGray, for: .normal)
        button.setTitleColor(.darkGray, for: .highlighted)
        button.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
        return button
    }()

    lazy var submitButton: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Submit", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(.lightGray, for: .highlighted)
        button.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
        button.backgroundColor = UIColor(red: 0x9C/255.0, green: 0x34/255.0, blue: 0xCA/255.0, alpha: 1)
        button.clipsToBounds = true
        button.layer.cornerRadius = 8
        button.contentEdgeInsets = .init(top: 8, left: 8, bottom: 8, right: 8)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .clear
        backgroundBlurView.alpha = 0.7

        [
            backgroundBlurView,
            contentView,
        ].forEach(view.addSubview(_:))

        [
            contentStackView,
        ].forEach(contentView.addSubview(_:))

        [
            titleLabel,
            slider,
            buttonsStackView,
        ].forEach(contentStackView.addArrangedSubview(_:))

        [
            cancelButton,
            submitButton,
        ].forEach(buttonsStackView.addArrangedSubview(_:))

        NSLayoutConstraint.activate([
            backgroundBlurView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundBlurView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            backgroundBlurView.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundBlurView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            contentView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),

            contentView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            contentView.centerYAnchor.constraint(equalTo: view.centerYAnchor),

            contentStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
            contentStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            contentStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 32),
            contentStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -32),

            buttonsStackView.widthAnchor.constraint(equalTo: contentStackView.widthAnchor, multiplier: 1, constant: -16),

            slider.widthAnchor.constraint(equalTo: contentStackView.widthAnchor),
        ])
    }

    func show(on viewController: UIViewController) {
        previousWindow = viewController.view.window
        guard let scene = previousWindow?.windowScene else {
            assertionFailure()
            return
        }

        let window = UIWindow(windowScene: scene)
        self.window = window
        window.rootViewController = self
        window.windowLevel = .alert
        window.makeKeyAndVisible()

        self.view.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.view.alpha = 1
        }
    }

    @objc private func submitButtonTapped() {
        dismissWindow()
    }

    @objc private func cancelButtonTapped() {
        dismissWindow()
    }

    private func dismissWindow() {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0
        }) { _ in
            self.window?.windowScene = nil
        }
    }
}
