//
//  ViewController.swift
//  window-test
//
//  Created by Denis Dzyubenko on 18/03/2020.
//  Copyright © 2020 Shortcut. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    class Cell: UITableViewCell {
        static let reuseIdentifier = "Cell"
    }

    lazy var feedbackViewController: FeedbackViewController = {
        return FeedbackViewController()
    }()

    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.dataSource = self
        tableView.register(Cell.self, forCellReuseIdentifier: Cell.reuseIdentifier)
        return tableView
    }()

    lazy var button: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Show feedback", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.backgroundColor = UIColor(red: 0x9C/255.0, green: 0x34/255.0, blue: 0xCA/255.0, alpha: 1)
        button.clipsToBounds = true
        button.layer.cornerRadius = 8
        button.contentEdgeInsets = .init(top: 8, left: 8, bottom: 8, right: 8)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .gray

        [
            tableView,
            button,
        ].forEach(view.addSubview(_:))

        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            button.centerYAnchor.constraint(equalTo: view.centerYAnchor),
        ])
    }

    @objc private func buttonTapped() {
        feedbackViewController.show(on: self)
    }
}

extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 999
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.reuseIdentifier, for: indexPath)
        cell.textLabel?.text = "Hello"
        cell.detailTextLabel?.text = "world"
        return cell
    }
}
